package pl.Arr.Training;

public class ArrayIteration {
    public static void main(String[] args) {

        final int[] myArr = {1,3,5,2,5,6,7,4,9,7};
        final int firstAndLastNumbers = 6;
        final int excludedNumber = 3;
        final int printToThisNumber = 5;

        printOut(myArr);
        firstXNumbersPrint(myArr,firstAndLastNumbers);
        lastXNumbersPrint(myArr,firstAndLastNumbers);
        evenNumbersPrint(myArr);
        backwardPrint(myArr);
        excludeNumberPrint(myArr,excludedNumber);
        printToSpecifiedNumber(myArr,printToThisNumber);


        //TODO
        //        forEachPrintOut(myArr);
        //How to cut last String --> arr[i] + " "


    }
    private static void printOut(int[] arr){
        System.out.print("Wszystkie liczby z tablicy: ");
        for(int i =0;i < arr.length;i++)
            System.out.print(arr[i] + " ");
        System.out.println();
    }
    private static void forEachPrintOut(int[] arr) {
        for (int i : arr)
            System.out.print(arr[i] + " ");
        System.out.println();
    }
    private static void firstXNumbersPrint(int[] arr,int border){
        System.out.print("Pierwsze " + border + " liczb: ");
        for(int i =0;i<border;i++)
            System.out.print(arr[i] + " ");
        System.out.println();
    }
    private static void lastXNumbersPrint(int[] arr,int border){
        System.out.print("Ostatnie " + border + " liczb: ");
        for(int i = arr.length-border;i<arr.length;i++)
            System.out.print(arr[i] + " ");
        System.out.println();
    }
    private static void evenNumbersPrint(int[] arr){
        System.out.print("Liczby parzyste z tablicy: ");
        for(int i = 0;i<arr.length;i++){
            if(arr[i] % 2 == 0)
                System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
    private static void backwardPrint(int[] arr){
        System.out.print("Wszystkie liczby z tablicy ale od końca: ");
        for(int i = arr.length-1 ; i>-1 ;i--)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    private static void excludeNumberPrint(int[] arr, int excl){
        System.out.print("Wycinam liczbę " + excl + " z tablicy: ");
        for(int i = 0;i<arr.length;i++){
            if(arr[i]!=excl)
                System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
    private static void  printToSpecifiedNumber(int[] arr,int toNumber){
        System.out.print("Drukuje tablicę chyba że znajdę liczbę " + toNumber + " wtedy zakończ: ");
        for(int i =0;i<arr.length;i++) {
            System.out.print(arr[i] + " ");
            if(arr[i] == toNumber){
                break;
            }
        }
    }
}
